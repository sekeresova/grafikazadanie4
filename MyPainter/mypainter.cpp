#include "mypainter.h"

MyPainter::MyPainter(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	ui.scrollArea->setWidget(&this->paintWidget);
	ui.scrollArea->setBackgroundRole(QPalette::Dark);
	paintWidget.newImage(500, 500);
}

MyPainter::~MyPainter()
{
	//delete[] TH;
	//delete[] blok;
}

void MyPainter::ActionOpen()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", "image files (*.png *.jpg *.bmp)");
	if (!fileName.isEmpty())
		paintWidget.openImage(fileName);
}

void MyPainter::ActionSave()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"), "untitled.png", tr("png Files (*.png)"));
	if (fileName.isEmpty()) {
		return;
	}
	else {
		paintWidget.saveImage(fileName);
	}
}

void MyPainter::EffectClear()
{
	paintWidget.clearImage();
	paintWidget.vymazBody();
}

void MyPainter::ActionNew()
{
	paintWidget.newImage(800, 600);
}

void MyPainter::Kresli()
{
	QPoint a, b;
	vector<QPoint> body;
	body = paintWidget.DajBody();
	body.push_back(body[0]);
	for (int i = 0; i < body.size() - 1; i++) {
		a = body[i];
		b = body[i + 1];
		paintWidget.DDA(a, b, ui.spinBox_2->value(), ui.spinBox_3->value(), ui.spinBox_4->value());
	}
	//paintWidget.naplnTH();
	//paintWidget.ScanLine();
}	

void MyPainter::ActionRotaciaVlavo()
{
	paintWidget.vymazZrotovaneBody();
	EffectClear();
	paintWidget.RotaciaVlavo(ui.lineEdit_znamienko->text(), ui.doubleSpinBox_hodnota->value());
	QPoint a, b;
	vector<QPoint> zrotovanyBod;
	zrotovanyBod = paintWidget.DajZrotovanyBod();
	for (int i = 0; i < zrotovanyBod.size() - 1; i++) {
		a = zrotovanyBod[i];
		b = zrotovanyBod[i + 1];
		paintWidget.DDA(a, b, ui.spinBox_2->value(), ui.spinBox_3->value(), ui.spinBox_4->value());
	}
}

void MyPainter::ActionRotaciaVpravo()
{
	paintWidget.vymazZrotovaneBody();
	EffectClear();
	paintWidget.RotaciaVpravo(ui.lineEdit_znamienko->text(), ui.doubleSpinBox_hodnota->value());
	QPoint a, b;
	vector<QPoint> zrotovanyBod;
	zrotovanyBod = paintWidget.DajZrotovanyBod();
	for (int i = 0; i < zrotovanyBod.size() - 1; i++) {
		a = zrotovanyBod[i];
		b = zrotovanyBod[i + 1];
		paintWidget.DDA(a, b, ui.spinBox_2->value(), ui.spinBox_3->value(), ui.spinBox_4->value());
	}
}

void MyPainter::ActionSkalovanie()
{
	paintWidget.clearImage();
	paintWidget.Skalovanie(ui.doubleSpinBox_skalovanie->value());
	vector<QPoint> skalovaneBody;
	skalovaneBody = paintWidget.DajSkalovanyBod();
	for (int i = 0; i < skalovaneBody.size() - 1; i++) {
		paintWidget.DDA(skalovaneBody[i], skalovaneBody[i + 1], ui.spinBox_2->value(), ui.spinBox_3->value(), ui.spinBox_4->value());
	}
}

void MyPainter::ActionSkosenie()
{
	//paintWidget.clearImage();
	paintWidget.Skosenie(ui.doubleSpinBox_skosenie->value(), ui.comboBox_skosenie->currentIndex());
	vector<QPoint> skosenyBod;
	skosenyBod = paintWidget.DajSkosenyBod();
	for (int i = 0; i < skosenyBod.size() - 1; i++) {
		paintWidget.DDA(skosenyBod[i], skosenyBod[i + 1], ui.spinBox_2->value(), ui.spinBox_3->value(), ui.spinBox_4->value());
	}
}

#ifndef MYPAINTER_H
#define MYPAINTER_H

#include <QtWidgets/QMainWindow>
#include "ui_mypainter.h"
#include "paintwidget.h"
#include <QElapsedTimer>
using namespace std;

class MyPainter : public QMainWindow
{
	Q_OBJECT

public:
	int velkost;
	MyPainter(QWidget *parent = 0);
	~MyPainter();

public slots:
	void ActionOpen();
	void ActionSave();
	void EffectClear();
	void ActionNew();
	void Kresli();
	void ActionRotaciaVlavo();
	void ActionRotaciaVpravo();
	void ActionSkalovanie();
	void ActionSkosenie();

private:
	Ui::MyPainterClass ui;
	PaintWidget paintWidget;
};

#endif // MYPAINTER_H

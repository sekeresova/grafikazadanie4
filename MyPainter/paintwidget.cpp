#include "paintwidget.h"


PaintWidget::PaintWidget(QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	modified = false;
	painting = false;
	myPenWidth = 5;
	myPenColor = Qt::black;
}

bool PaintWidget::openImage(const QString &fileName)
{
	QImage loadedImage;
	if (!loadedImage.load(fileName))
		return false;

	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::newImage(int x, int y)
{
	QImage loadedImage(x,y,QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::saveImage(const QString &fileName)
{
	QImage visibleImage = image;
	resizeImage(&visibleImage, size());

	if (visibleImage.save(fileName,"png")) {
		modified = false;
		return true;
	}
	else {
		return false;
	}
}

void PaintWidget::setPenColor(const QColor &newColor)
{
	myPenColor = newColor;
}

void PaintWidget::setPenWidth(int newWidth)
{
	myPenWidth = newWidth;
}

void PaintWidget::clearImage()
{
	image.fill(qRgb(255, 255, 255));
	modified = true;
	update();
}

void PaintWidget::mousePressEvent(QMouseEvent *event)
{
	QPainter painter(&image);
	if (event->button() == Qt::LeftButton) {
		painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		lastPoint = event->pos();
		painting = true;
		body.push_back(lastPoint);
		painter.drawPoint(lastPoint);
	}
	update();
}

void PaintWidget::mouseDoubleClickEvent(QMouseEvent *event)
{
	QPainter painter;
	if ((event->buttons() & Qt::LeftButton)) {
		lastPoint = event->pos();
		drawLineTo(event->pos());
	}
}

/* zapis vsetkych bodov pocas tahu */
void PaintWidget::mouseMoveEvent(QMouseEvent *event)
{
	QPainter painter;
	if ((event->buttons() & Qt::LeftButton) && painting) {
		drawLineTo(event->pos());
		lastPoint = event->pos();
		bodyPosun.push_back(lastPoint);
		printf("bodyTest SOM V moveEvent: %d\n", bodyPosun[bodyPosun.size() - 1]);
	}
	
}

/* zapis bodov pri skonceni tahu */
void PaintWidget::mouseReleaseEvent(QMouseEvent *event)
{
	QPainter painter;
	if (event->button() == Qt::LeftButton && painting) {
		lastPoint = event->pos();
	}
}

void PaintWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void PaintWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

void PaintWidget::drawLineTo(const QPoint &endPoint)
{
	QPainter painter(&image);
	painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	modified = true;
	update();
}

void PaintWidget::resizeImage(QImage *image, const QSize &newSize)
{
	if (image->size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), *image);
	*image = newImage;
}

void PaintWidget::vymazBody()
{
	body.clear();
}

void PaintWidget::vymazZrotovaneBody()
{
	zrotovanyBod.clear();
}

bool sortFunkciaTH(const QVector<float>& a, const QVector<float>& b)
{
	if (a[1] == b[1]) return a[0] < b[0];
	else if (a[1] == b[1] && a[0] == b[0]) return a[3] < b[3];
	else return a[1] < b[1];
}

void PaintWidget::naplnTH()
{
	//printf("SOM V TABULKE HRAN");
	//system("PAUSE");
	stlpec = 4;
	float dx, dy;
	riadok = body.size();
		for (int i = 0; i < riadok; i++) {

			QVector<float> row;
			dx = abs(body[i + 1].x() - body[i].x());
			dy = abs(body[i + 1].y() - body[i].y());
			if (dy != 0) {
				w = (dx / dy);
				row.push_back(body[i].x() + w);		//Xz
				row.push_back(body[i].y() - 1);		//Yz	
				row.push_back(body[i + 1].y());		//Yk
				row.push_back(w);		//w - obratena hodnota smernice
				TH.push_back(row);
			}
		}
		qSort(TH.begin(), TH.end(), sortFunkciaTH);
}

/* Sortovanie TAH podla x-priesecnikov */
bool sortFunkciaTAH(const QVector<float>& a, const QVector<float>& b)
{
	return a[4] < b[4];
}

void PaintWidget::ScanLine()
{
	int pocet = 0;
	QPainter painter(&image);
	float priesecnikX;
	//aktualny presecnik na Xovej suradnici
	QVector<QVector<float>> TAH;
	float Ya = TH[0][1];
	float Ymax = TH[riadok - 1][1];

	while (Ya <= Ymax) {
		//do TAH zaradim harny ktore, Ya = Yz, musim prejst celu tabulku hran
		for (int i = 0; i < riadok; i++) {
			QVector<float> row;
			if (TH[i][1] == Ya) {
				row.push_back(TH[i][0]);		//Xz
				row.push_back(TH[i][1]);		//Yz
				row.push_back(TH[i][2]);		//Yk
				row.push_back(TH[i][3]);		//w - obratena hodnota smernice
				//pridame dalsiu info a to je Xovy priesecnik na aktualnom riadku Ya
				//Xa = W*Ya - W*Yz + Xz
				priesecnikX = (TH[i][3] * Ya) - (TH[i][3] * TH[i][1]) + TH[i][0];
				row.push_back(priesecnikX);
				TAH.push_back(row);
				pocet++;
				TAH.resize(pocet);
			}
		}
		qSort(TAH.begin(), TAH.end(), sortFunkciaTAH);
		
		/* Pospajanie priesecnikov */
		for (int i = 0; i <= TAH.size() - 2; i = i + 2) {
			/* TAH[i][1] = Ya */
			painter.drawLine(round(TAH[i][4]), round(TAH[i][1]), round(TAH[i + 1][4]), round(TAH[i][1]));
		}
		/* Vymazanie hrany z TAH ak Yk == Ya */
		for (int i = 0; i < TAH.size(); i++) {
			for (int j = 0; j < 5; j++) {
				if (TAH[i][2] == Ya) {
					TAH[i].erase(TAH[i].begin() + j);
					pocet--;
					TAH.resize(pocet);
				}
			}
		}
		Ya++;
	}
}

void PaintWidget::DDA(QPoint a, QPoint b, int red, int green, int blue)
{
	QPainter painter(&image);
	color.setRgb(red, green, blue);
	painter.setPen(QPen(color, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.setBrush(QBrush(color));

	int pocet = body.size();
	float  m, xx, yy, deltaY, deltaX, X, Y;
	int x1, x2, y1, y2;
	x1 = a.x();
	y1 = a.y();
	x2 = b.x();
	y2 = b.y();
	for (int i = 0; i < body.size() - 1; i++) {
		deltaX = x2 - x1;
		deltaY = y2 - y1;
		if (fabs(deltaY) <= fabs(deltaX)) {
			m = (deltaY / deltaX);
			if (x1 < x2) { xx = x1; yy = y1; X = x2; }
			else { xx = x2; yy = y2; X = x1; }
			painter.drawEllipse((int)xx, (int)yy, 3, 3);
			while (xx < X) {
				xx = xx + 1.0;
				yy = yy + m;
				painter.drawEllipse(xx, (int)yy, 3, 3);
			}
		}
		else {
			m = (deltaX / deltaY);
			if (y1 < y2) { xx = x1; yy = y1; Y = y2; }
			else { xx = x2; yy = y2; Y = y1; }
			painter.drawEllipse((int)xx, (int)yy, 3, 3);
			while (yy < Y) {
				xx = xx + m;
				yy = yy + 1.0;
				painter.drawEllipse((int)xx, yy, 3, 3);
			}
		}
	}
	update();
}

void PaintWidget::Posunutie()
{
	/*najst priesecnik s hranou a vyratat rozdiel o ktory sa treba posunut*/
	/* y = kx + q, mam dve rovnice o dovh neznamych, prva rovnica hrany a druha rovnica tahu mysou */
	int rozdielX = bodyPosun[bodyPosun.size() - 1].x() - bodyPosun[0].x();
}

void PaintWidget::RotaciaVlavo(QString znamienko, double hodnota)
{
	int Sx = body[0].x();
	int Sy = body[0].y();
	int X, Y;
	zrotovanyBod.push_back(body[0]);
	for (int i = 1; i < body.size(); i++) {
		if (znamienko == ("*")) {
			X = round(((body[i].x() - Sx) * cos(-M_PI * hodnota) - (body[i].y() - Sy) * sin(-M_PI * hodnota) + Sx));
			Y = round(((body[i].x() - Sx) * sin(-M_PI * hodnota) + (body[i].y() - Sy) * cos(-M_PI * hodnota) + Sy));
			zrotovanyBod.push_back(QPoint(X, Y));
		}
		else {
			X = round(((body[i].x() - Sx) * cos(-M_PI / hodnota) - (body[i].y() - Sy) * sin(-M_PI / hodnota) + Sx));
			Y = round(((body[i].x() - Sx) * sin(-M_PI / hodnota) + (body[i].y() - Sy) * cos(-M_PI / hodnota) + Sy));
			zrotovanyBod.push_back(QPoint(X, Y));
		}
	}
	zrotovanyBod.push_back(zrotovanyBod[0]);
}

void PaintWidget::RotaciaVpravo(QString znamienko, double hodnota)
{
	int Sx = body[0].x();
	int Sy = body[0].y();
	int X, Y;
	zrotovanyBod.push_back(body[0]);
	for (int i = 1; i < body.size(); i++) {
		if (znamienko == ("*")) {
			X = round(((body[i].x() - Sx) * cos(-M_PI * hodnota) + (body[i].y() - Sy) * sin(-M_PI * hodnota) + Sx));
			Y = round( ((body[i].x() - Sx) * sin(M_PI * hodnota) + (body[i].y() - Sy) * cos(M_PI * hodnota) + Sy));
			zrotovanyBod.push_back(QPoint(X, Y));
		}
		else {
			X = round(((body[i].x() - Sx) * cos(-M_PI / hodnota) + (body[i].y() - Sy) * sin(-M_PI / hodnota) + Sx));
			Y = round(((body[i].x() - Sx) * sin(M_PI  / hodnota) + (body[i].y() - Sy) * cos(M_PI / hodnota) + Sy));
			zrotovanyBod.push_back(QPoint(X, Y));
		}
	}
	zrotovanyBod.push_back(zrotovanyBod[0]);
}

void PaintWidget::Skalovanie(double koeficient)
{
	int X, Y, rozdielX, rozdielY;
	skalovanyBod.push_back(body[0]);
	for (int i = 1; i < body.size(); i++) {
		rozdielX = body[i].x() - body[0].x();
		rozdielY = body[i].y() - body[0].y();
		X = round(koeficient * rozdielX + body[i].x());
		Y = round(koeficient * rozdielY + body[i].y());
		skalovanyBod.push_back(QPoint(X,Y));
	}
	skalovanyBod.push_back(body[0]);
}

void PaintWidget::Preklopenie()
{

}

void PaintWidget::Skosenie(double d, double smer)
{
	int X, Y;
	if (smer == 0) {
		skosenyBod.push_back(body[0]);
		for (int i = 1; i < body.size(); i++) {
			X = round(body[i].x() + (d * body[i].y()));
			Y = body[i].y();
			skosenyBod.push_back(QPoint(X,Y));
		}
		skosenyBod.push_back(skosenyBod[0]);
	}
	else if (smer == 1) {
		skosenyBod.push_back(body[0]);
		for (int i = 0; i < body.size(); i++) {
			X = body[i].x();
			Y = round(body[i].y() + (d * body[i].x()));
			skosenyBod.push_back(QPoint(X,Y));
		}
		skosenyBod.push_back(body[0]);
	}
}